[ABANDONED]

# Epigrafo
Epigrafo is a cross-platfrom journaling app that aims to provide a feature-rich yet beautiful user-experience while remaining easy to use. It's currently under development and is planned to support Windows, MacOS, Linux, Android and iOS.

# Getting Started
To get started with Epigrafo, follow these steps.
## Installation
You can find a binary distribution on the site, ready to install, or you can compile from source. To do that:
```sh
git clone https://gitlab.com/memoryerror/epigrafo.git
cd epigrafo
python3 setup.py
```