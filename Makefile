all:
	pyi-makespec epigrafo/main.py --path=epigrafo --onefile
	pyinstaller main.spec
	cp -r epigrafo/assets dist/
	cp -r epigrafo/ui dist
	rm dist/ui/*.py
