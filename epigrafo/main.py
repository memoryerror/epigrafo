"""isort:skip_file"""
from kivy.app import App
from kivy.config import Config
from kivy.resources import resource_add_path
from kivy.uix.screenmanager import ScreenManager

Config.set("graphics", "resizable", False)
from ui.auth import Auth
from ui.editor import Editor
from ui.menu import Menu
from ui.view import View

resource_add_path("assets")


class Screens(ScreenManager):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.add_widget(Auth(name="Auth"))
        self.add_widget(Menu(name="Menu"))
        self.add_widget(View(name="View"))
        self.add_widget(Editor(name="Editor"))


class ImpressionApp(App):
    def build(self):
        return Screens()


if __name__ == "__main__":
    ImpressionApp().run()
