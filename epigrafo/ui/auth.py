from pathlib import Path

from kivy.lang.builder import Builder
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.screenmanager import Screen
from kivy.uix.textinput import TextInput

from core import entry

style = str(Path("ui/styles/auth.kv"))
Builder.load_file(style)


class Auth(Screen):
    def login(self, password):
        menu = self.manager.get_screen("Menu")
        menu.password = password
        menu.previews = self.create_previews(password)
        self.manager.current = "Menu"

    def create_previews(self, password):
        entry_manager = entry.EntryManager(password)
        entries = entry_manager.get_entries()
        previews = [
            {"title": entry.title, "content": entry.content} for entry in entries
        ]
        return previews
