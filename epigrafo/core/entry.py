import os
import sqlite3
from datetime import datetime

import appdirs
from cryptography.fernet import InvalidToken
from recordclass import recordclass

from core.crypt import Crypter

Entry = recordclass("Entry", "id timestamp title content")


class EntryManager:
    def __init__(self, key):
        default_data_dir = appdirs.user_data_dir("impression")
        db_location = os.path.join(default_data_dir, ".userdata.db")
        self.db = sqlite3.connect(db_location)
        self.key = key
        self.crypter = Crypter(key)
        self.db.execute(
            """CREATE TABLE IF NOT EXISTS entries(
            ID INTEGER PRIMARY KEY,
            Timestamp VARCHAR(25),
            Title BLOB,
            Content BLOB);
            """
        )

    def new(self, title, content):
        timestamp = str(datetime.now())
        title, content = self.crypter.encrypt(title, content)
        self.db.execute(
            """INSERT INTO entries(Timestamp, Title, Content)
            VALUES(?, ?, ?);
            """,
            (timestamp, title, content),
        )
        self.db.commit()

    def edit(self, id, title=None, content=None):
        # Get the title and content.
        current_title, current_content = self.get_entry_by_id(id)[-2:]
        # Don't change the value if value is not given.
        title = self.crypter.encrypt(title) if title else current_title
        content = self.crypter.encrypt(content) if content else current_content

        self.db.execute(
            """UPDATE entries
            SET Title = "{0}", Content = "{1}"
            WHERE ID = {2}""".format(
                title, content, id
            )
        )
        self.db.commit()

    def delete(self, id):
        self.db.execute("DELETE FROM entries WHERE ID = " + str(id))
        self.db.commit()

    def get_entries(self):
        entries_raw = self.db.execute("SELECT * FROM entries").fetchall()
        # Convert all of the entries to namedtuples.
        entries = [Entry(*entry_raw) for entry_raw in entries_raw]
        for entry in entries:
            entry.title, entry.content = self.crypter.decrypt(
                entry.title.encode(), entry.content.encode()
            )
        return entries

    def get_entry_by_id(self, id):
        entry_raw = self.db.execute(
            "SELECT * FROM entries WHERE ID = " + str(id)
        ).fetchone()
        # Convert the tuple into a namedtuple.
        entry = Entry(*entry_raw)
        entry.title, entry.content = self.crypter.decrypt(
            entry.title.encode(), entry.content.encode()
        )
        return entry

    def change_pass(self, old, new):
        # In case you find this confusing: First we replace the key with the old one provided by the user, and try to decrypt
        # the entries with that key. By default, if the key is invalid, an error is raised, and the key is verified.
        # Of course, this isn't completely fool-proof, but nothing is. :)
        try:
            self.crypter = Crypter(old)
            entries = self.get_entries()
        except InvalidToken as msg:
            self.crypter = Crypter(self.key)
            raise InvalidToken(msg)

        self.crypter = Crypter(new)
        for entry in entries:
            self.delete(entry.id)
            self.new(entry.title, entry.content)
